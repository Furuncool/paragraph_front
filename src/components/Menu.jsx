import React from 'react';
import styled from 'styled-components';
import Vector from '../images/Vector.svg';
import ButtonAdd from '../images/button_add.svg';
import Pen from '../images/icons/big/pen.svg';
import Book from '../images/icons/big/book.svg';
import Chest from '../images/icons/big/chest.svg';
import Doska from '../images/icons/big/doska.svg';
import Journal from '../images/icons/big/journal.svg';

const StyledMenu = styled.nav`
  width: 142px;
  background-color: #fff;
  margin-right: auto;
  padding-top: 22px;

`

const MenuItems = styled.ul`
  width: 100%;
  display: flex;
  align-items: center;
  gap: 82px;
  flex-direction: column;
`

const MenuItem = styled.a`
  
`

const Menu = () => {
  return (
    <StyledMenu>
      <MenuItems>
        <li><MenuItem href='#'><img src={Vector}/></MenuItem></li>
        <li><MenuItem href='#'><img src={ButtonAdd}/></MenuItem></li>
        <li><MenuItem href='#'><img src={Chest}/></MenuItem></li>
        <li><MenuItem href='#'><img src={Pen}/></MenuItem></li>
        <li><MenuItem href='#'><img src={Book}/></MenuItem></li>
        <li><MenuItem href='#'><img src={Journal}/></MenuItem></li>
        <li><MenuItem href='#'><img src={Doska}/></MenuItem></li>
      </MenuItems>
    </StyledMenu>
  );
};

export default Menu;