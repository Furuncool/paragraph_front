import React from 'react';
import styled from 'styled-components';
import Button from './Button';

const StyledMain = styled.main`
  width: 990px;
  display: flex;
  flex-direction: column;
  margin-right: auto;
  padding-top: 38px;
`

const MainHead = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 46px;
`

const LeftColumnHead = styled.div`
  display: flex;
  flex-direction: column;
  gap: 17px;
`

const ListNameBlock = styled.div`
  display: flex;
  gap: 22px;
  font-size: 22px;
  align-items: flex-end;

  p{
    color: #224973;
  }
`

const ListName = styled.p`
  color: #9498A0;
`

const ListDescriptionBlock = styled.div`
  display: flex;
  gap: 22px;
  font-size: 14px;

  p{
    color: #224973;
  }
`

const ListDescription = styled.p`
  color: #9498A0;
`

const HeadButtons = styled.div`
  display: flex;
  gap: 18px;
`

const MainWorkBlock = styled.div`
  width: 100%;
  background-color: #fff;
  /* УБРАТЬ НИЖЕ */
  height: 1000px;  
`

const Main = () => {
  return (
    <StyledMain>
      <MainHead>
        <LeftColumnHead>
          <ListNameBlock>
            <p>Список</p>
            <ListName>Задания с квадратными уравнениями</ListName>
          </ListNameBlock>
          <ListDescriptionBlock>
            <p>Описание списка</p>
            <ListDescription>Сейчас будет весело</ListDescription>
          </ListDescriptionBlock>
        </LeftColumnHead>

        <HeadButtons>
          <Button>Посмотреть</Button>
          <Button secondary>Сохранить</Button>
        </HeadButtons>
      </MainHead>


      <MainWorkBlock></MainWorkBlock>
    </StyledMain>
  );
};

export default Main;